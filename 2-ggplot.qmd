---
title: "Block 2: Intro to dataviz with ggplot2"
subtitle: "Let's talk about R"
author: "Iñigo Urrestarazu-Porta"
host: "IKER"
institute: "CNRS -- IKER UMR5478, UPV/EHU, UPPA"
date: "today"
date-format: "DD-MM-YYYY"
format:
  revealjs:
    theme: assets/css/iker-slides.scss
    # logo: assets/figs/logo-iker.png
    slide-number: c
    show-slide-number: all
    code-link: true
    code-copy: hover
    chalkboard: true
    pointer: true
    touch: true
    pdf-separate-fragments: true
    hide-inactive-cursor: true
revealjs-plugins:
  - pointer
output-location: slide
fig-align: center
preview-links: true
link-external-newwindow: false
title-slide-attributes: 
  data-background-image: assets/figs/logos/logo-cnrs.png, assets/figs/logos/logo-iker-color.png, assets/figs/logos/logo-uppa.jpg, assets/figs/logos/logo-ehu-gardena-2.png
  data-background-size: 3.5%, 7%, 7%, 8%, 7%
  data-background-position: 71.5% 95%, 79% 94.5%, 87.5% 95%, 95% 95%
width: 1600
fig-width: 16
fig-height: 8
height: 900
execute:
  echo: true
#   out.width: "100%"
#   fig.asp: 0.5625
#   dpi: 300

editor: source
engine: knitr
---

## Data visualization is a basic element of data analysis

<br>

- Data visualization as an [exploration]{.emph} tool

<br>

- Data visualization as a tool for [understanding model output]{.emph}

<br>

- Data visualization as a [communication]{.emph} tool

## Goals
<br>
<br>
Understand the basic functioning of ggplot

<br>
Apply good dataviz recommendations

## Requierements

### Tidyverse
```{r}
#| output-location: default
# install.packages("tidyverse)
library(tidyverse)
library(patchwork)
my.colors <- RColorBrewer::brewer.pal(n = 8, name = "Accent")
accent <- "#59a7dc"
```

### Data
```{r}
#| output-location: default
source("simulated-categorical-data.R")
source("simulated-continuous-data.R")
```

##

![](assets/figs/allison-horst/allison-horst-ggplot2.png){.r-stretch fig-align="center"}

::: footer
Artwork by @[allison_horst](https://allisonhorst.com/)
:::

## The Grammar of Graphics structure

![](assets/figs/fig-ggplot-basic-structure.png){.absolute right=0 bottom=80 height="350"}

::: footer
[Source: [https://www.science-craft.com/2014/07/08/introducing-the-grammar-of-graphics-plotting-concept/](https://www.science-craft.com/2014/07/08/introducing-the-grammar-of-graphics-plotting-concept/)]{style="font-size: 10px;"}
:::

## The Grammar of Graphics structure

![](assets/figs/fig-ggplot-full-structure.png){.absolute right=0 bottom=80 height="500"}

::: footer
[Source: [https://www.science-craft.com/2014/07/08/introducing-the-grammar-of-graphics-plotting-concept/](https://www.science-craft.com/2014/07/08/introducing-the-grammar-of-graphics-plotting-concept/)]{style="font-size: 10px;"}
:::

{{< include 2-ggplot/slides-01-first-plot.qmd >}}

```{r}
#| echo: false

theme_set(theme_light())
```


{{< include 2-ggplot/slides-02-geom-density-histogram.qmd >}}

{{< include 2-ggplot/slides-03-remove-expand.qmd >}}

{{< include 2-ggplot/slides-04-labs.qmd >}}

## Easy peasy!

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExcGx2M3lsOTMxaW5jam5xdHVyeWd3MHBhdGF3b3RjMHhuM2Q0M3I1ZyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/7voye1pl3ndQSjwGnY/giphy.gif){.r-stretch fig-align="center"}

{{< include 2-ggplot/slides-05-categorical-data.qmd >}}

{{< include 2-ggplot/slides-continuous-data.qmd >}}

{{< include 2-ggplot/slides-geom_line.qmd >}}


{{< include 2-ggplot/slides-colors.qmd >}}

{{< include 2-ggplot/slides-facets.qmd >}}

{{< include 2-ggplot/slides-patchwork.qmd >}}

{{< include 2-ggplot/slides-theming.qmd >}}




## Take-home messages

1. Think about your plots

2. Vertical comparison is tends to be easier to process than horizontal comparison

3. Show the whole distributions

4. Show the n of tokens

5. Be consistent about theming and coloring

6. Make sure you plots are accessible for color-blind people

7. Remove non-data ink within reason




## Sources and beyond

Chang, Winston. 2023. *R Graphics Cookbook*. O'Reilly. [r-graphics.org/](https://r-graphics.org/)

<br>

Wickham, Hadley; Navarro, Danielle & Pedersen, Thomas L. 2023. *ggplot2: elegant graphics for data analysis*. [ggplot2-book.org](https://ggplot2-book.org/)

<br>

ggplot extension gallery: [https://exts.ggplot2.tidyverse.org/gallery/](https://exts.ggplot2.tidyverse.org/gallery/)





---

{{< include r-final-slide.qmd >}}



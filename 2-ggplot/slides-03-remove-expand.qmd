
## Ggplot leaves some annoying space in the margins {auto-animate="true"}

```{r}
#| fig-align: center
df %>% 
  ggplot(aes(x = rt)) +
  geom_histogram(aes(x = rt, after_stat(density)), fill = NA, color = "black") +
  geom_density(fill = NA, color = "red", linewidth = 1) +
  theme_light() +
  scale_y_continuous(expand = c(0, 0))
```

## Ggplot leaves some annoying space in the margins {auto-animate="true"}

```{r}
#| fig-align: center
df %>% 
  ggplot(aes(x = rt)) +
  geom_histogram(aes(x = rt, after_stat(density)), fill = NA, color = "black") +
  geom_density(fill = NA, color = "red", linewidth = 1) +
  theme_light() +
  scale_y_continuous(expand = c(0, 0.0001))
```

## Ggplot leaves some annoying space in the margins {auto-animate="true"}

```{r}
#| fig-align: center
df %>% 
  ggplot(aes(x = rt)) +
  geom_histogram(aes(x = rt, after_stat(density)), fill = NA, color = "black") +
  geom_density(fill = NA, color = "red", linewidth = 1) +
  theme_light() +
  scale_y_continuous(expand = c(0, 0.0001), limits = c(0, 0.014))
```

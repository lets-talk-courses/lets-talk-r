
# Modeling skewed continuous data: reaction times {.transition}

## Fitting the model

## Checking model assumptions

## Summary

## Extracting population level effects

## Extracting random effects

## Understanding the results

# Communicating our results {.transition}

## Writing results

## Plotting results


# Beyond the intercept only model (II) {.transition}
Multiple independent variables

##

```{r}
distributions.by.village <- l.continuous.data %>% 
  mutate(phone = factor(phone, levels = c("z", "s", "x", "tz", "ts", "tx"))) %>% 
  ggplot(aes(group = phone, y = village, x = cog)) +
    ggdist::stat_halfeye(
      aes(fill = phone),
    adjust = .5, 
    width = .6, 
    justification = -.2, 
    .width = 0, 
    point_colour = NA
    ) + 
  geom_boxplot(
    inherit.aes = FALSE,
    aes(y = village, x = cog, color = phone),
    width = .15,
    outlier.color = NA
  ) +
  geom_text(
    data = l.continuous.data %>% 
      group_by(village, phone) %>% 
      summarise(x = mean(cog)) %>% 
      mutate(y = 1:6),
    mapping = aes(
      x = x,
    label = village,
    y = village),
    vjust = -2.5,
    size = 4,
    color = "white"
  ) +
  scale_fill_manual(values = my.colors[c(1:3, 5:7)]) +
  scale_color_manual(values = my.colors[c(1:3, 5:7)]) +
  theme(legend.position = "none",
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank()) +
  labs(
    x = "Center of Gravity (Hz)",
    y = NULL
  )

distributions.by.village +
    facet_wrap(~phone, nrow = 2,
             ncol = 3) 
```


## Fitting the model
```{r}
mdl.phone.village <- lm(cog ~ 1 + phone + village, data = l.continuous.data)
```

# Checking model assumptions

## q-q plot
```{r}
mdl.phone.village %>% 
  ggplot(aes(sample = cog)) +
  stat_qq() +
  geom_qq_line() +
  labs(x = "Theoretical quantiles",
       y = "Sample quantiles")
```

## Residuals plot
```{r}
broom::augment(mdl.phone.village) %>% 
  ggplot(aes(x = .resid)) +
  geom_histogram() +
  geom_vline(xintercept = mean(resid(mdl.phone.village)),
             linetype = "dashed",
             color = accent,
             linewidth = 1) +
  scale_y_continuous(expand = c(0,0), limits = c(-10, 5500)) +
  labs(x = "Distribution of residuals",
       title = "Now it looks so much better",
       subtitle = "but still there is some tail to the left")
```

## Summary
```{r}
summary(mdl.phone.village)
```

## Extracting estimates
```{r}
tidy(mdl.phone.village, conf.int = TRUE) %>% 
  gt()

phone.vill.estimates <- tidy(mdl.phone.village, conf.int = TRUE) %>% 
  select(term, estimate, contains("conf"))

gt(phone.vill.estimates)
```

```{r}
emmeans(mdl.phone.village, ~ phone + village)
```

```{r}
estimates.to.plot <- ggemmeans(mdl.phone.village, terms = c("phone", "village")) %>% as_tibble() %>% 
  rename(phone = x, village = group)

estimates.to.plot %>% gt()
```

```{r}
emmeans(mdl.phone.village, specs = c("phone", "village"), contr =  "pairwise")
```

## Understanding the results
```{r}
distributions.by.village +
  geom_point(
    inherit.aes = FALSE,
    data = estimates.to.plot,
    aes(x = predicted,
        y = village,
        color = phone)
  ) +
  geom_linerange(
    inherit.aes = FALSE,
    data = estimates.to.plot,
    aes(
      y = village,
      color = phone,
      xmin = conf.low,
      xmax = conf.high)
  ) +
  facet_wrap(~ phone) +
  labs(title = "Data distribution and model estimates")
```

##
```{r}
estimates.to.plot %>% 
  ggplot(aes(y = phone, x = predicted, color = phone)) +
  geom_point() +
  geom_linerange(
    aes(xmin = conf.low, xmax = conf.high),
    linewidth = 2.5
  )
```


# Communicating our results {.transition}

## Writing results

## Plotting results

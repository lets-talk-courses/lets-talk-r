---
title: "Block 1: Intro to R"
subtitle: "Let's talk about R"
author: "Iñigo Urrestarazu-Porta"
host: "IKER"
institute: "CNRS -- IKER UMR5478, UPV/EHU, UPPA"
date: "today"
date-format: "DD-MM-YYYY"
format:
  revealjs:
    theme: assets/css/iker-slides.scss
    # logo: assets/figs/logo-iker.png
    slide-number: c
    show-slide-number: all
    code-link: true
    chalkboard: true
    pointer: true
    touch: true
    pdf-separate-fragments: true
    hide-inactive-cursor: true
revealjs-plugins:
  - pointer
preview-links: true
link-external-newwindow: false
title-slide-attributes: 
  data-background-image: assets/figs/logos/logo-cnrs.png, assets/figs/logos/logo-iker-color.png, assets/figs/logos/logo-uppa.jpg, assets/figs/logos/logo-ehu-gardena-2.png
  data-background-size: 3.5%, 7%, 7%, 8%, 7%
  data-background-position: 62.5% 95%, 70% 94.5%, 79% 95%, 87% 95%, 95% 95%
width: 1600
height: 900
execute:
  echo: true
#   out.width: "100%"
#   fig.asp: 0.5625
#   dpi: 300

editor: source
engine: knitr
---

```{r}
#| echo: false
library(tidyverse)
```


{{< include 1-slides-intro-to-r/slides-what-is-r.qmd >}}

{{< include 1-slides-intro-to-r/slides-install-r-rstudio.qmd >}}

{{< include 1-slides-intro-to-r/slides-basic-operations.qmd >}}


{{< include 1-slides-intro-to-r/slides-project-qmd.qmd >}}

{{< include 1-slides-intro-to-r/slides-data-types.qmd >}}


{{< include 1-slides-intro-to-r/slides-functions.qmd >}}

{{< include 1-slides-intro-to-r/slides-beyond-base-r.qmd >}}




# It is okay to feel panic and overwhelmed {.transition}

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExejd5NWxoZmZqYm50Z25jNXN0NXQ0aXJxczNpaHk1emt1bTJ1cmQ5ZSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/KmTnUKop0AfFm/giphy.gif){.r-stretch fig-align="center"}






## Just remember

![](assets/figs/fig-using-r.jpeg){.r-stretch fig-align="center"}

---

{{< include r-final-slide.qmd >}}















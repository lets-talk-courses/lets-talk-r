---
title: "Block 4: Basic tidyverse"
subtitle: "Let's talk about R"
author: "Iñigo Urrestarazu-Porta"
host: "IKER"
institute: "CNRS -- IKER UMR5478, UPV/EHU, UPPA"
date: "today"
date-format: "DD-MM-YYYY"
format:
  revealjs:
    theme: assets/css/iker-slides.scss
    # logo: assets/figs/logo-iker.png
    slide-number: c
    show-slide-number: all
    code-link: true
    code-copy: hover
    chalkboard: true
    pointer: true
    touch: true
    pdf-separate-fragments: true
    hide-inactive-cursor: true
revealjs-plugins:
  - pointer
output-location: slide
fig-align: center
preview-links: true
link-external-newwindow: false
title-slide-attributes: 
  data-background-image: assets/figs/logos/logo-cnrs.png, assets/figs/logos/logo-iker-color.png, assets/figs/logos/logo-uppa.jpg, assets/figs/logos/logo-ehu-gardena-2.png
  data-background-size: 3.5%, 7%, 7%, 8%, 7%
  data-background-position: 71.5% 95%, 79% 94.5%, 87.5% 95%, 95% 95%
width: 1600
fig-width: 16
fig-height: 8
height: 900
execute:
  echo: true
#   out.width: "100%"
#   fig.asp: 0.5625
#   dpi: 300

editor: source
engine: knitr
---

## Goals

- Discover the Tidyverse

- Get an overview of the possibilities it offers

## Resources

Packages:

```{r}
#| output-location: default
# install.packages("tidyverse")
library(tidyverse)
my.colors <- RColorBrewer::brewer.pal(n = 8, name = "Accent")
accent <- "#59a7dc"
```

Data:
```{r}
#| output-location: default
continuous.data <- read_csv("https://gitlab.com/urrestarazu/teaching-stats/-/raw/main/data/continuous-data.csv")
```

##

![](assets/figs/allison-horst/allison-horst-tidyverse.png){.r-stretch fig-align="center"}

::: footer
Artwork by @[allison_horst](https://allisonhorst.com/)


{{< include 4-slides-tidyverse/slides-read-data.qmd >}}
{{< include 4-slides-tidyverse/slides-df-tibble.qmd >}}
{{< include 4-slides-tidyverse/slides-filter.qmd >}}
{{< include 4-slides-tidyverse/slides-pipe.qmd >}}
{{< include 4-slides-tidyverse/slides-select.qmd >}}
{{< include 4-slides-tidyverse/slides-pivot.qmd >}}

{{< include 4-slides-tidyverse/slides-dplyr-summarize.qmd >}}
{{< include 4-slides-tidyverse/slides-dplyr-count.qmd >}}
{{< include 4-slides-tidyverse/slides-dplyr-arrange.qmd >}}
{{< include 4-slides-tidyverse/slides-dplyr-group-by.qmd >}}

{{< include 4-slides-tidyverse/slides-dplyr-mutate.qmd >}}
{{< include 4-slides-tidyverse/slides-dplyr-pull-distinct.qmd >}}

{{< include 4-slides-tidyverse/slides-dplyr-binding.qmd >}}
{{< include 4-slides-tidyverse/slides-dplyr-join.qmd >}}

{{< include 4-slides-tidyverse/slides-stringr.qmd >}}



---

{{< include r-final-slide.qmd >}}

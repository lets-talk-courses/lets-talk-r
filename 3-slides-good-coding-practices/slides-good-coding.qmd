
# Good coding practices {.transition}

## The fewer characters you type, the better

`alt / option` + `-`: <-

`ctr / cmd` + `shift` + `M`: %>% 

`ctr / cmd` + `alt / option` + `I`: code chunk

`alt / option` + `up / down arrows`: move line(s) up / down

`ctr / cmd` + `alt / option` + `up / down arrows`: duplicate line(s) up / down

`ctr / cmd` + `enter`: run code line(s) and move to next commands

`alt / option` + `enter`: run code line(s) and stay in the same position 

`ctr / cmd` + `shit` + `enter`: run chunk / entire script

Use `tab` key:

- In chunk to display functions

- Between quotes to access files

- In functions to display options


## Keep it simple, keep it small

00-simulating-data.R

01-power-analysis.R

02-load-data.R

03-data-wrangling.R

04-summary-statistics.R

05-exploratory-dataviz.R

06-modeling.R

07-model-output.R

08-model-plot.R


\>\>\>\>\> 


my-one-and-only-file-and-analysis.R


## Don't trust your memory

Comment your code

Really, comment your code

Really, always

## How to comment the code

1. [Purpose]{.emph} of the chunk

2. [Why]{.emph} are you doing it

3. [Why]{.emph} are you doing it this way (and not that other way)

4. What you tried / thought and discarded, and [why]{.emph}

5. You should not comment what the code does

## Commented code
```{r}
#| eval: false
#| output-location: default

## Import data
data <- read.csv(
  file = "path-to-file.csv",
  header = FALSE, # in the csv there are no column titles
  col.names = c("id", "village", "age.group"),  # supply column names here,
                                                # in order not to rename them later on
  sep = ";",    # the software generated a ; separated file
                # (bc language settings)
  dec = ",", # decimals were separated by ",", not "."
  na.strings = "NULL" # the software generated NULL
                      # for non-available values
                      # without this, NULL values have to be
                      # turned into NAs later on
)
```

## Load just what you need

::: notes

This will save processing memory

Protect the pc in the long run

Make it smoother in the short run

Save power

:::

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExaWIzZGhhdjU0MXA0bmd5MTJxbG1pNTRvNGV3ZXQzODh3NHRoajlrYyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/l0HU1hasfJKRST93q/giphy.gif){.r-stretch fig-align="center"}

## Load just what you need (in the file that you need)

Avoid this:

```{r}
#| eval: false
#| output-location: default

library(tidyverse) # manipulate data, pipe, ggplot...
library(lme4) # modeling
library(ggeffects) # extracting model results
library(emmeans) # extracting model results
library(ggbeeswarm) # for ploting the data
library(broom.mixed) # extracting tidy model results
library(effsize) # get effect size estimates
library(simr) # simulate power from data
```

## Load just what you need:

What do I need for this file? Am I loading something I don't need?

How many times am I using this package (in this file)?

If n < 3, consider not loading the package and calling the function via the package

```{r}
#| eval: false
#| output-location: default

tidy.results <- broom.mixed::tidy(mdl)
```


## Remove what you don't need any more

Specially if they are big

```{r}
#| eval: false
#| output-location: default
summary.statistics <- data %>% 
  group_by(village) %>% 
  summarize(
    tokens = n(cog),
    mean.cog = mean(cog),
    median.cog = median(cog),
    sd.cog = sd(cog),
    min.cog = min(cog),
    max.cog = max(cog),
    iqr.cog = IQR(cog)
  )

rm(summary.statistics)
```


## If you are going the multi-script way,

Make each script [self-contained]{.emph}

[Restart R session]{.emph} between scripts


## Use placeholders

Do this:

```{r}
#| eval: false
#| output-location: default


n.part <- 20

sample(c("a", "b"), replace = TRUE, size = n.part)

for (i in 1:length(n.part)) {
  ...
}
```

Instead of this:

```{r}
#| eval: false
#| output-location: default

sample(c("a", "b"), replace = TRUE, size = 20)

for (i in 1:length(20)) {
  ...
}
```

## Trust me, you don't want to try to spot each time where that number is used

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExNm85bXJhYWdleWt6anUwbnk4ZGhhdDM0OXFyMDd5dmFkeDE4bzQ5ZyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/26n6WywJyh39n1pBu/giphy.gif){.r-stretch fig-align="center"}

## Because you won't spot them all

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExdWphMDlzNjZxeW5jcnFxcXlqb2U3OGg4cWYzZnlnZG9xbHBmeXRnOCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/FEAngqNOOfnMGOzOTk/giphy.gif){.r-stretch fig-align="center"}


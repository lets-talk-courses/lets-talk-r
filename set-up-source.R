library(tidyverse)
library(lme4)
library(lmerTest)
library(broom.mixed)
library(patchwork)
library(gt)
my.colors <- RColorBrewer::brewer.pal(n = 8, name = "Accent")
accent <- "#59a7dc"

# To access it online
# https://gitlab.com/urrestarazu/teaching-stats/-/raw/main/set-up-source.R

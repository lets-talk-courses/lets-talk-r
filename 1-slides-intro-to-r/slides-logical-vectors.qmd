# Logical vectors {.transition}

## What are logical vectors?

![](https://media.giphy.com/media/2UoK8hKjl3RttTRFVQ/giphy-downsized-large.gif){.r-stretch fig-align="center"}

# What can we do with logical vectors? {.transition}

## We can count them
```{r}
x <- seq(1, 10, by = 1)

x.l <- x > 5

summary(x.l)
```

Once we have counts, we have numbers!!!

## Logical vectors are really powerful because we can use them to filter observations

```{r}
x[which(x > 6)]
```



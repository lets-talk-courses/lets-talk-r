# Numeric and integer vectors {.transition}

## Numbers!!!

```{r}
#| echo: true
x <- seq(0, 10, by = 0.1)
x
```

# What can we do with numeric vectors? {.transition}

## We can arrange them

```{r}
sort(x)
```

Sure, it was already arranged

## We can arrange them

```{r}
sort(x, decreasing = TRUE)
```


## Calculate the mean

```{r}
#| echo: true

mean(x)

```

## Calculate the standard deviation

```{r}
#| echo: true

sd(x)
```

## Calculate the median

```{r}
#| echo: true

median(x)
```

## Calculate the median

```{r}
#| echo: true

quantile(x, 0.5)

```

## Calculate the median

```{r}
#| echo: true

quantile(x, 0.5)

```

## Calculate the median

```{r}
#| echo: true

median(x) == quantile(x, 0.5)
```

## Calculate the minimum

```{r}
#| echo: true

min(x)
```


## Calculate the maximum

```{r}
#| echo: true

max(x)
```

## Calculate the range

```{r}
#| echo: true

range(x)
```

## Calculate the IQR

```{r}
#| echo: true
IQR(x)
```

## Transformations

```{r}
x + 20
```


## Transformations

```{r}
x * 20
```

## Transformations

```{r}
x - mean(x)
```

## Transformations

```{r}
(x - mean(x)) / sd(x)
```

## Check a condition

```{r}
x > 6
```




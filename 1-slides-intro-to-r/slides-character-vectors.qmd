# Character vectors {.transition}

## What are character vectors?

```{r}
x <- as.character(x)

summary(x)
```

# What can we do with character vectors? {.transition}

## We can sort them (kind of)
```{r}
sort(x)
```

## We can sort them (kind of)
```{r}
sort(x, decreasing = TRUE)
```

## Evaluate conditions
```{r}

x == "1"

x == c("1", "2")

```

## Filter observations based on conditions
```{r}
x[which(x == "1")]
```


## Replace observations based on conditions (this actually applies to all vectors)
```{r}
x[which(x == "1")]  <- "a"
x
```


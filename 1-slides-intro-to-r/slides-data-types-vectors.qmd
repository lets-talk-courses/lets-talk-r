
# Vectors {.transition}

## What are vectors?

```{r}
#| echo: true
x <- seq(1, 10, by = 1)
x
length(x)
x[1]
x[11]
x[2:3]
x[c(4, 8, 10)]
```


## Types of vectors in R and what we can do with each of them

- Numeric vectors

- Integer vectors

- Logical vectors

- Character vectors

- Factor vectors

- Date vectors (we're not seeing these, but check `lubridate` package)


{{< include 1-slides-intro-to-r/slides-numeric-vectors.qmd >}}
{{< include 1-slides-intro-to-r/slides-logical-vectors.qmd >}}
{{< include 1-slides-intro-to-r/slides-factor-vectors.qmd >}}
{{< include 1-slides-intro-to-r/slides-character-vectors.qmd >}}

# Data frames {.transition}

## What are data frames

Sets of vectors, for which the positions are related.

Let's create one

## Creating a data frame

```{r}
speaker <- paste0("sp_", 1:10)
speaker
```

## Creating a data frame
```{r}
age <- round(seq(25, 42, length.out = 10))
age
```

## Creating a data frame

```{r}
df <- data.frame(speaker, age)
df
```

## Adding a new column
```{r}
village <- rep(c("altsasu", "urdiain"), 5)
village
```

## Adding a new column
```{r}
df$village <- village
df
```

## Adding a new column
```{r}
df$village <- rep(c("altsasu", "urdiain"), 5)
df
```

# What can we do with data frames {.transition}
We can do (almost) anything with data frames


## Select columns

```{r}
df$speaker

```

## Select columns
```{r}
df[1]
```

## Select columns
```{r}
df[,1]
```


## Select columns
```{r}
df[c(1, 3)]
```

## Filter rows
```{r}
df[1,]
```

## Filter rows based on conditions
```{r}
df[which(df$speaker == "sp_1"),]
```

## Summarize the data frame
```{r}
summary(df)
```

## Summarize a column
```{r}
summary(df[1])
```

## Make histograms of columns
```{r}
hist(df$age)
```


## Create new columns
```{r}
df$c_age <- df$age - mean(df$age)
df
```

## Create new ones
```{r}
urdiain <- c(mean = mean(df$age[which(df$village == "urdiain")]),
  median = median(df$age[which(df$village == "urdiain")]),
  sd = sd(df$age[which(df$village == "urdiain")]),
  min = min(df$age[which(df$village == "urdiain")]),
  max = max(df$age[which(df$village == "urdiain")]),
  range = max(df$age[which(df$village == "urdiain")]) - min(df$age[which(df$village == "urdiain")]))
```


## Create new ones
```{r}
altsasu <- c(mean = mean(df$age[which(df$village == "altsasu")]),
  median = median(df$age[which(df$village == "altsasu")]),
  sd = sd(df$age[which(df$village == "altsasu")]),
  min = min(df$age[which(df$village == "altsasu")]),
  max = max(df$age[which(df$village == "altsasu")]),
  range = max(df$age[which(df$village == "altsasu")]) - min(df$age[which(df$village == "urdiain")]))
```

## Combine data frames
```{r}
rbind(urdiain, altsasu)
```

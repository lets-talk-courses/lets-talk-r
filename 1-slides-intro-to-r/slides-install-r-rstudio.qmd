
# Installing R

. . .

[OS specific](https://cran.r-project.org)


## Installing R in Linux[: in Ubuntu]{.fragment}

. . .

<iframe src="https://brew.sh" style="border:none;" height="800" width="100%"></iframe>

## Installing R in Linux: in Arch derivates

<iframe src="https://wiki.archlinux.org/title/r" style="border:none;" height="800" width="100%"></iframe>


## Installing R in MacOS

<iframe src="https://brew.sh" style="border:none;" height="800" width="100%"></iframe>

## Installing R in Windows

Rtools & R

<iframe src="https://cran.r-project.org/" style="border:none;" height="600" width="100%"></iframe>

## Let's check the install

In Windows: CMD (yes, the black thing that looks like the Matrix)

In others: open terminal

Type: [R]{.emph}

# It's better with an Integrated Development Environment (IDE) {.transition}

Let's install RStudio

## What's RStudio

It's a desktop application

It's also FOSS

Developed by the Posit PBC (formerly RStudio PBC)

## Installing RStudio in Linux: Ubuntu

<iframe src="https://brew.sh" style="border:none;" height="800" width="100%"></iframe>

## Installing RStudio in Linux: Arch-derivates

<iframe src="https://aur.archlinux.org/packages/rstudio-desktop" style="border:none;" height="800" width="100%"></iframe>

## Installing RStudio in MacOS

<iframe src="https://brew.sh" style="border:none;" height="800" width="100%"></iframe>

## Installing RStudio in Windows

<iframe src="https://posit.co/downloads/" style="border:none;" height="800" width="100%"></iframe>

## Basic layout

![](assets/figs/fig-rstudio-first-layout.png){.r-stretch fig-align="center"}

## Let's change some of the default settings



## We don't work with the console

One function at a time

No record of what we do

No comments on the code
